#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
     print("Python Loops")
     print("\nProgram requirements:\n"
        +"1. Print while loop\n"
        +"2. Print for loops using range() function, and implicit and explicit lists.\n"
        +"3. Use break and continue statements\n"
        +"4. Replicate display below\n")


def while_loop_1():
  print("1. while loop:")
  x = 1
  while x < 4:
    print(x)
    x = x + 1
  print()

def for_loop_2():
  print("2. for loop: using range() function with 1 arg")
  c = range(4)
  for n in c:
    print(n)
  print()


def for_loop_3():
  print("3. for loop: using range() function with two args")
  v = range(1, 4)
  for t in v:
    print(t)
  print()

def for_loop_4():
  print("4. for loop: using range() function with three args (interval 2):")
  n = range(1, 4, 2)
  for h in n:
    print(h)
  print()

def for_loop_5():
  print("5. for loop: using range() function with three args (negative interval):")
  j = range(3, 0, -2)
  for k in j:
    print(k)
  print()

def for_loop_6():
  print("6. for loop: using (implicit) list (i.e., list not assigned to variable):")
  for i in [1,2,3]:
    print(i)
  print()

def for_loop_7_8_9_10():
  print("7. for loop: using explicit list of strings:")
  states = ["Michigan", "Alabama","Florida"]
  for state in states:
    print(state)
  print()

  print("8. for loop: using break statement(stops loop):")
  for state in states:
    if state == "Alabama":
      break
    print(state)
  print()

  print("9. for loop: using continue statement (stops and continue with next):")
  for state in states:
    if state == "Alabama":
      continue
    print(state)
    print()

  print("10. print list length:")
  print(len(states))
  print()