#!/usr/bin/env python3
import functions as f

# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

def main():
    f.get_requirements()
    f.while_loop_1()
    f.for_loop_2()
    f.for_loop_3()
    f.for_loop_4()
    f.for_loop_5()
    f.for_loop_6()
    f.for_loop_7_8_9_10()


if __name__ == "__main__":
    main()