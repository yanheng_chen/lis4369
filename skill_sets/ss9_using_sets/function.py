#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
     print("Python sets")
     print("\nProgram requirements:\n"
        +"1. Sets (Python data structure): mutable. hterogeneous, unodredered sequence of elements, *but* cannot hold duplicate values.\n"
        +"2. Sets are mutable/changeable--that is, can insert, update, delete.\n"
        +"3. Whiles sets are mutable/changeable, they *cannot* contain other mutable items like list, set, or dictionary--\n"
        +"     that is, elements contained in sets must be immutable."
        +"4. Also, since sets are unordered, cannot use indexing(or, slicing) to access, update, or delete elements.\n"
        +"5. Two methods to create sets: \n"
        +"      a. Create sets using curly brackets {set}: = {1, 3.14, 2.0, 'four', 'Five'}\n"
        +"      b. Create set using set() function: my_set = set(<iterable>) \n"
        +"      Examples: \n"
        +"      my_set1 = set([1, 3.14, 2.0, 'four', 'Five']) # set with list \n"
        +"      my_set2 = set((1, 3.14, 2.0. 'four', 'Five')) # set with tuple \n"
        +"      Note: An \"iterable\" is *any* object, which can be ietrated over--that is, lists, tuples, or even strings. \n"
        +"6. Create a program that mirrors the following IPO(input/process/output) format. \n")




def sets():
    print("Input: Hard coded--no user input.")
    print("***Note***: All three sets below print as \"sets\" (i.e., curly brackets), *regardless* of how they were created!")

    print()
    print("Print my_set created using curly brackets: ")
    my_set1 = {1, 2.0, 3.14, 'Five', 'four'}
    print(my_set1)
    
    print()

    print("print type of my_set")
    print(type(my_set1))

    print()
    print("Print my_set2 created using set() function with tuple")
    my_set2 = set([1, 2.0, 3.14, 'four', 'Five'])
    print(my_set2)

    print()
    print("print type of my_set2")
    print(type(my_set2))

    print()

    print("length of my_set")
    print(len(my_set1))

    print()

    print("Discard 'four'")
    my_set1.discard("four")
    print(my_set1)

    print()

    print("Remove 'Five'")
    my_set1.remove("Five")
    print(my_set1)

    print()

    print("Length of my_set")
    print(len(my_set1))

    print()

    print("Add element to set (4) using add() method:")
    my_set1.add(4)
    print(my_set1)

    print()

    print("Display minimum numner:")
    print(min(my_set1))

    print()

    print("Display maximum numner:")
    print(max(my_set1))

    print()

    print("Display sum of numbers: ")
    print(sum(my_set1))

    print()

    print("Delete all set elements: ")
    my_set1.clear()
    print(my_set1)

    print()

    print("Length of my_set: ")
    print(len(my_set1))
