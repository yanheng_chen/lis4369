#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
    print("Square feet to acres")
    print("\nProgram Requirements:\n"
        + "1. Research: number of square feet to acre of land. \n"
        + "2. Must use float data type for user input and calculation. \n"
        + "3. Format and round conversion to two decimal places.")




def get_feet_to_acres():
    print("\nUser input: ")
    square_feet = float(input("Enter square feet: "))
#convert square feet to acres 
    ACRES_TO_SQUARE_FEET = 43560
    acres = round(square_feet / ACRES_TO_SQUARE_FEET, 2)
#print out the processed feet with two decimal places
    print("\nProgram output ")
    print("{0:,.2f}".format(square_feet),"square feet is", "{0:,.2f}".format(acres), "acres.")