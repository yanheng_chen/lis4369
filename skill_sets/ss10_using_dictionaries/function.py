#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
from typing import MutableMapping


def get_requirements():
     print("Python Dictionaries")
     print("\nProgram requirements:\n"
        +"1. Dictionaries (Python data structure): unordered key: value pairs.\n"
        +"2. Dictionary: an associative array (also known as hashes).\n"
        +"3. Any key in a dirctioary is associated (or mapped) to a value (i.2., any Python data type).\n"
        +"4. Keys: must be of immutable type (string number or tuple with immutable elements) and must be unique.\n"
        +"5. Values: can be any data type and can repeat. \n"
        +"6. Create a program that mirrors the following IPO(input/process/output) format. \n"
        +"      Create empty dictionary, using only curly braces {}: my_dictionary = {}\n"
        +"      Use the following keys: fname, lname, degree, major, gpa \n"
        +"Note: Dictionaries have key-value pairs instead of single values; this differentiates a dictionary from a set. \n")


def get_dictionaries():

    print("Input: \n")

    my_dictionary = {}

    my_dictionary = {
        "fname": input("First Name: "),
        "lname": input("Last Name: "),
        "degree": input("Degree: "),
        "major": input("Major (IT or ICT): "),
        "gpa": input("GPA (Better be good): ")
    }


    print("Output \n")

    print("Print my_dictionary:")
    print(my_dictionary)

    print()

    print("Return view of dictionary's (key, value) paor, built-in function:")
    print(my_dictionary.items())

    print()

    print("Return view objects of all keys, built-in function: ")
    print(my_dictionary.keys())

    print()

    print("Return view object of all values in dictionary, built-in function: ")
    print(my_dictionary.values())

    print()

    print("Print only first and last names, using keys: ")
    print(my_dictionary["fname"], my_dictionary["lname"])

    print()

    print("Print only fist and last names, using get() function: ")
    print(my_dictionary.get("fname"), my_dictionary.get("lname"))

    print()

    print("Count number of item (key: value pairs) in dictionary:")
    print(len(my_dictionary))

    print()

    print("Remove last dictionary item (popitem):")
    my_dictionary.popitem()
    print(my_dictionary)

    print()

    print("Delete major from dictionary, using key:")
    del my_dictionary["major"]
    print(my_dictionary)

    print()

    print("Return object type: ")
    print(type(my_dictionary))

    print()

    print("Delete all items of list: ")
    my_dictionary.clear()
    print(my_dictionary)