#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
from typing import MutableMapping

import math
import operator
import os, sys, stat


def get_requirements():
     print("File Write Read")
     print("\nProgram requirements:\n"
        +"1. Create write_read_file subdirectory with two files: main.py and functions.py.\n"
        +"2. Use President Abraham Lincoln's Gettysburg Address: Full Text.\n"
        +"3. Write address to file.\n"
        +"4. Read address from same file.\n"
        +"5. Create Python Docstrings for functions in functions.py file.\n"
        +"6. Display Python Docstrings for each function in functions.py file.\n"
        +"7. Display full file path.\n"
        +"8. Replicate display below")

def write_read_file():
    '''Usage: Calls two functions:
         1. file_write() # writes to file
         2. file_read() # reads from file
       Parameters: None
       Returns: None
    '''
    file_write()
    file_read()

def file_write():
   '''Usage: creates file, and writes contents fo global variable to file
       Parameters: none
       Returns: none
   '''

   log_dir = os.path.join('C:/Users/cyh21/repos/lis4369/skill_sets/ss15_file_write_read/write_read_file', 'address.txt')
   fh = open(log_dir, 'w')
    

   fh.write("Four score and seven years ago our fathers brought forth on this continent, \n"
   + "a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal.\n"
   + "\n"
   + "Now we are engaged in a great civil war, testing whether that nation, or any\n"
   + "nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a\n"
   + "final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this.\n"
   + "\n"
   + "But, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow -- this ground. The brave men, living and dead, who struggled here,\n"
   + "have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what\n"
   + "they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced.\n"
   + "It is rather for us to be here dedicated to the great task remaining before us -- that from these honored dead we take increased devotion to that cause for\n"
   + "which they gave the last full measure of devotion -- that we here highly resolve that these dead shall not have died in vain -- that this nation, under God, shall\n"
   + "have a new birth of freedom -- and that government of the people, by the people, for the people, shall not perish from the earth.\n"
   + "\n"
   + "Abraham Lincoln\n"
   + "November 19, 1863\n"
   )
   fh.close()

def file_read():
    '''Usage: reads contents of written file
       Parameters: none
       Returns: none
    '''

    log_dir = os.path.join('C:/Users/cyh21/repos/lis4369/skill_sets/ss15_file_write_read/write_read_file', 'address.txt')
    fh = open(log_dir)

    print(fh.read())

    print(os.path.abspath(log_dir))

    fh.close()