 #!/usr/bin/env python3
import function as f



# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

def main():
    f.get_requirements()
    print()
    print("Help on function write_read_file \n")
    print(f.write_read_file.__doc__)
    print()
    print("Help on function file_write \n")
    print(f.file_write.__doc__)
    print("Help on function file_read \n")
    print()
    print(f.file_read.__doc__)
    f.write_read_file()



if __name__ == "__main__":
    main()