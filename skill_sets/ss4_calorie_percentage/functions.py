#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
     print("Calories Calculator")
     print("\nProgram requirements:\n"
        +"1. Find calories per grams of fat, carbs, and protein\n"
        +"2. Calculate percentages\n"
        +"3. Must use float data types.\n"
        +"4. Format, right-align numbers, and round to two decimal places \n")



def calories_convert():
    FAT_CAL = 9
    CARB_CAL = 4
    PROTEIN_CAL = 4

    fat = float(input("Enter total fat grams: "))
    carb = float(input("Enter total carb grams: "))
    protein = float(input("Enter total protein grams: "))


    cal_fat = fat * FAT_CAL
    cal_carb = carb * CARB_CAL
    cal_protein = protein * PROTEIN_CAL

    total_cal = cal_fat + cal_carb + cal_protein
    fat_percentage = round(cal_fat / total_cal * 100, 2)
    carb_percentage = round(cal_carb / total_cal * 100, 2)
    protein_percentage = round(cal_protein / total_cal * 100, 2)

    format(cal_fat,cal_carb,cal_protein,fat_percentage, carb_percentage, protein_percentage)


def format(cal_f, cal_c, cal_p, fat, carb, protein):
    print("Type{}Calories{}Percentage".format("\t","\t"))
    print("Fat\t","{0:,.2f}".format(cal_f),"\t","{0:,.2f}".format(fat))
    print("Carb\t","{0:,.2f}".format(cal_c),"\t","{0:,.2f}".format(carb))
    print("Protein\t","{0:,.2f}".format(cal_p),"\t","{0:,.2f}".format(protein))