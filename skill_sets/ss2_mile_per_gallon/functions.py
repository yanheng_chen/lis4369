#!/usr/bin/env python3

# Program requirements:


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

def get_requirements():
    print("Miles per gallon")
    print("\nProgram Requirements:\n"
        + "1. Convert MPG \n"
        + "2. Must use float data type for user input and calculation. \n"
        + "3. Format and round conversion to two decimal places.")


def mpg():
#input
    print("\nUser Input")
    miles_driven = float(input("Enter miles driven: "))
    gallons_used = float(input("Enter gallons of fuel used: "))
#process by dividing miles driven with gallons used
    mpg = round(miles_driven / gallons_used, 2)
#output
    print("\nOutput")
    print("{0:,.2f}".format(miles_driven), "miles driven and", "{0:,.2f}".format(gallons_used), "gallons used =", mpg, "mpg")