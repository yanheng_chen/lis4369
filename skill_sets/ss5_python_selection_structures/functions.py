#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
     print("Python Selection Structures")
     print("\nProgram requirements:\n"
        +"1. Use Python selection structure\n"
        +"2. Prompt user for two numbers, and a suitable operator\n"
        +"3. Test for correct numeric operator\n"
        +"4. Replicate display below\n")



def get_input():
# get number and operator from user
    print("Python Calculator")
    num1 = input("Enter num1:")
    num2 = input("Enter num2:")
    print("\nSuitable Operators: +,-,*,/,//(integer division, %(modulo operator), **(power)")
    operator = input("Enter operator:")
#check to see input is valid
    if num1.isnumeric() == True and num2.isnumeric() == True:
        n1 = float(num1)
        n2 = float(num2)
        operate(n1, n2, operator)
    else:
        print("Enter numbers not language")

def operate(num1, num2, operator):
#check if the operator is valid, if it is, do the operation, if not, then just print "Illegal operator"
    if operator == "+":
        num3 = num1 + num2
        print(num3)
    elif operator == "-":
        num3 = num1 - num2
        print(num3)
    elif operator == "*":
        num3 = num1 * num2
        print(num3)
    elif operator == "/":
        if (num2 == 0):
            print("Can't divide by zero")
        else:
            num3 = num1 / num2
            print(num3)
    elif operator == "//":
            if (num2 == 0):
                print("Can't divide by zero")
            else:
                num3 = num1 // num2
                print(num3)
    elif operator == "%":
        if (num2 == 0):
                print("Can't divide by zero")
        else:
            num3 = num1 % num2
            print(num3)
    elif operator == "**":
        num3 = num1 ** num2
        print(num3)
    else:
        print("Illegal operator!")