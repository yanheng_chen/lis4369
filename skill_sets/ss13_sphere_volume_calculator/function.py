#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
from typing import MutableMapping

import math


def get_requirements():
     print("Sphere Volume Program")
     print("\nProgram requirements:\n"
        +"1. Program calculates sphere volume in liquid U.S. gallons from user-centered diameter values in inches, and rounds to two decimal places.\n"
        +"2. Must use Python's built in PI and pow() capabilities.\n"
        +"3. Program checks for non-integers and non-numeric values.\n"
        +"4. Program continues to prompt for user entry until no longer requested, prompt accepts upper or lower case letters.\n")


def check_user_input(num):
    try:
        val = int(num)
        return True
    except ValueError:
        return False


def sphere_volume(num):
    pie=math.pi
    radian = num / 2
    num2 = 4 / 3
    num4 = pow(radian, 3) * pie
    volume = round((num2 * num4) / 231, 2)
    print("Volume is: ", volume, "US Lquid Gallons (Why can't we just use the metric system?")

    again = input("\nDo you want to do another one? Enter yes or no: ")
    while again.lower() != "yes":
        if again.lower() == "no":
            print("Thank you for using our Sphere Volume Calculator")
            break;
        if again.lower() != "no":
            print("Enter again!")
            again = input("\nDo you want to do another one? Enter yes or no: ")
    else:
        check_user_input2()


def check_user_input2():
    num = input("\nEnter an integer for your diameter: ")
    while check_user_input(num) == False:
        print("invalid integer, try again")
        num = input("\nEnter an integer: ")
        check_user_input(num)
    else:
        num = int(num)
        sphere_volume(num)

