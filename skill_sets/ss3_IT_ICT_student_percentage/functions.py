#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
    print("Miles per gallon")
    print("\nProgram Requirements:\n"
        + "1. Find number of IT/ICT students in class \n"
        + "2. Calculate IT/ICT student percentage \n"
        + "3. Must use float data type (to facilitate right-alignment) \n"
        + "4. Format, right-align number, and round to two decimal places")



#input
def get_percentage():
    print("\ninput")
    num_IT_stu = float(input("Enter number of IT students: "))
    num_ICT_stu = float(input("Enter number of ICT students: "))
#process
    total_stu = num_IT_stu + num_ICT_stu
    IT_stu = float(round(num_IT_stu / total_stu * 100, 2))
    ICT_stu = float(round(num_ICT_stu/ total_stu * 100, 2))
#output
    print("\nTotal students: ", "{0:,.2f}".format(total_stu))
    print("IT students: ", "{0:>8}%".format(IT_stu))
    print("ICT students: ","{0:>7}%".format(ICT_stu))