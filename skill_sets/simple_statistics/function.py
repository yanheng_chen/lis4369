#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
import statistics as s


def get_requirements():
  print("Python Simple Statistics")

  print("\nProgram Requirements:\n"
     + "1. Create stats subdirectory with two files: main.py and functions.py.\n"
     + "2. Create integer list (see below).\n"
     + "3. Create user-defined get_range() function.\n"
     + "4. Use Python intrinsic (built-in) functions from \"statistics\" module.\n")


