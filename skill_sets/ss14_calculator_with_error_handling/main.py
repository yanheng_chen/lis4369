 #!/usr/bin/env python3
import function as f



# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

def main():
    f.get_requirements()
    f.get_input()



if __name__ == "__main__":
    main()