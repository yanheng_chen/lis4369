#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
from typing import MutableMapping

import math
import operator


def get_requirements():
     print("Python Calculator with Error Handling")
     print("\nProgram requirements:\n"
        +"1. Program calculates two numbers, and rounds to two decimal places.\n"
        +"2. Prompts user for two numbers, and a suitable operator.\n"
        +"3. Use python error handling to validate data.\n"
        +"4. Test for correct arithmetic calculator.\n"
        +"5. Division by zer0 not permitted.\n"
        +"6. Note: Program loops until correct input entered -numbers and arithmetic operator.\n"
        +"7. Replicate display below.\n")


def check_user_input(num):
    try:
        val = float(num)
        return True
    except ValueError:
        return False

def check_operator(op):
    allowed_operators={
        "+": operator.add,
        "-": operator.sub,
        "*": operator.mul,
        "/": operator.truediv,
        "**": operator.pow,
        "%" : operator.mod,
        "//": operator.floordiv}
    try:
        result=allowed_operators[op](10,9)
        return True
    except:
        return False

def get_input():
# get number and operator from user and validate them before processing

    print("Python Calculator with error handling")
    num1 = input("\nEnter num1: ")
    while check_user_input(num1) == False:
        print("Invalid number.")
        num1 = input("\nEnter num1: ")
    else:
        num1 = int(num1)
    
    num2 = input("\nEnter num2: ")
    while check_user_input(num2) == False:
        print("Invalid number.")
        num2 = input("\nEnter num2: ")
    else: 
        num2 = int(num2)

    print("\nSuitable Operators: +,-,*,/,//(integer division, %(modulo operator), **(power)")
    operator = input("Enter operator:")

#check to see if operator is valid
    while check_operator(operator) != True:
        print("Invalid operator, try again\n")
        print("\nSuitable Operators: +,-,*,/,//(integer division, %(modulo operator), **(power)")
        operator = input("Enter operator:")
    else:
        operate(num1, num2, operator)

    
    


def operate(num1, num2, operator):
#check if the operator is valid, if it is, do the operation, if not, then just print "Illegal operator"
    if operator == "+":
        num3 = num1 + num2
        print(num3)
    elif operator == "-":
        num3 = num1 - num2
        print(num3)
    elif operator == "*":
        num3 = num1 * num2
        print(num3)
    elif operator == "/":
            while (num2 == 0):
                print("Can't divide by zero\n")
                num2 = input("\nEnter num2: ")
                while check_user_input(num2) == False:
                    print("Invalid number.")
                    num2 = input("\nEnter num2: ")
                else: 
                    num2 = int(num2)
            else:
                num3 = num1 / num2
                print(num3)
    elif operator == "//":
            while (num2 == 0):
                print("Can't divide by zero\n")
                num2 = input("\nEnter num2: ")
                while check_user_input(num2) == False:
                    print("Invalid number.")
                    num2 = input("\nEnter num2: ")
                else: 
                    num2 = int(num2)
            else:
                num3 = num1 // num2
                print(num3)
    elif operator == "%":
            while (num2 == 0):
                print("Can't divide by zero\n")
                num2 = input("\nEnter num2: ")
                while check_user_input(num2) == False:
                    print("Invalid number.")
                    num2 = input("\nEnter num2: ")
                else: 
                    num2 = int(num2)
            else:
                num3 = num1 % num2
                print(num3)
    elif operator == "**":
        num3 = num1 ** num2
        print(num3)
    else:
        print("Illegal operator!")
    
    print()

    print("Thank you for using our Python Calculator with error handling")
    
