#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
from typing import MutableMapping


def get_requirements():
     print("Temperature Conversion Program")
     print("\nProgram requirements:\n"
        +"1. Program converts user-entered temperatures into Fahrenheit or Celsius scales.\n"
        +"2. Program continues to prompt for user entry until no longer requested.\n"
        +"3. Note: upper or lower case letter permitted. Though, incorrect entries are not permitted.\n"
        +"4. Note: Program does not validate numeric data (optional requirement).\n")


def conversion():
    print()
    print("Output:")
    type = input("Fahrenheit to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"c\": ")

    while type != "f" and type != "c":
        print("Incorrect entry, try again")
        type = input("Fahrenheit to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"c\": ")
    
    if type == "f":
        num = input("Enter temperature in Fahrenheit: ")
        if num.isnumeric():
            num = int(num)
            
            numerator = (num - 32) * 5

            final = round(numerator / 9, 2)

            print("Temperature in Celsius: ", final)

        else:
            print("Enter a number like you learned in Kindergarten.")
    
    elif type == "c":
        num = input("Enter temperature in Celsius: ")
        if num.isnumeric():
            num = int(num)

            num2 = round(1.8 * num, 1)

            final = num2 + 32

            print("Temperature in Fahrenheit: ", final)

        else:
            print("Enter a number like you learned in Kindergarten")