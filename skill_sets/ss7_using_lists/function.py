#!/usr/bin/env python3


# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
     print("Python Lists")
     print("\nProgram requirements:\n"
        +"1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
        +"2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
        +"3. Create list - using square brackets [list]: my_list = [\"cherries\", \"apples\", \"bananas\", \"organes\"].\n"
        +"4. Create a program that can mirrors the following IPO (input/process/order) format.\n")
    
     print("note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run).")


def lists():
    list_num = int(input("Enter number of list elements: "))
    x = 1
    my_list = []
    print()
    
    while x < list_num + 1:
        y = str(x)
        my_list.append(input("Please enter list element " + y + ": "))
        x += 1
    
    print()

    print("Output:")
    print("Print my_list")
    print(my_list)

    print()
    
    new_item = input("Please enter list element: ")
    new_index = int(input("Please enter list *index*(so 0 is first) postition: "))

    print()

    print("Insert element into specific postion in my_list")
    my_list.insert(new_index, new_item)
    print(my_list)

    print()

    print("Count number of elements in list:")
    print(len(my_list))

    print()
    print("Sort elements in list alphabetically: ")
    my_list.sort()
    print(my_list)

    print()
    print("Reverse list: ")
    my_list.reverse()
    print(my_list)

    print()
    print("Remove last item from list")
    my_list.pop()
    print(my_list)

    print()
    print("Remove item from list using index")
    my_list.pop(int(input("Enter the index of the element you want to remove: ")))
    print(my_list)

    print()
    print("Delete list element by value")
    get_out = input("enter the value you want to delete: ")
    my_list.remove(get_out)
    print(my_list)

    print()
    print("Delete all element from list")
    my_list.clear()
    print(my_list)