> **NOTE:** This README.md file should be placed at the **root of each of your repos directories!**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Assignment 5 Requirements:

*Four Parts:*

1. Complete Introduction_to_R_Setup_and_Tutorial and A5 requirements and save as learn_to_use_r.R
2. Code and run lis4369_a5.R and include the link to a5 README.md file
3. Include at least 2 4-panel RStudio screenshots for learn_to_use_r.R and lis4369_a5.R
4. Include at least four plots, two for each tutorial and put it in the README.md file


> ##### Skill sets 7, 8, and 9:

|      Skill set 13 | Skill set 14  |  Skill set 15     |
| ----------- | ----------- |---------------------- |
|    ![Skill_set_13 Sphere Volume Calculator](img/ss_13.png) |   ![Skill_set_14 Calculator with error handling](img/ss_14.png)     |![Skill_set_15 File Write Read](img/ss_15_1.png)
|   |        |  ![Skill_set_15 File Write Read](img/ss_15_2.png) 




#### Assignment Screenshots:

*Screenshot of A5 Data Analysis Using R:


Screenshot of Data Analysis Using R   
![A5_Learn_to_Use_R](img/learn_to_use_r.png "Screenshot of Data Analysis Using R ")
![A5_Learn_to_Use_R_plots](r_tutorial/default_plot.png "Plot from learn_to_use_r file ")
![A5_Learn_to_Use_R_plots](r_tutorial/myplot.jpg "Plot from learn_to_use_r file ")
![A5_lis4369_A5](img/lis4369_a5.png "Screenshot of Data Analysis Using R")
![A5_lis4369_A5_plots](lis4369_a5.R/plot1.png "Plot from lis4369_a5 file ")
![A5_lis4369_A5_plots](lis4369_a5.R/plot2.png "Plot from lis4369_a5 file ")

