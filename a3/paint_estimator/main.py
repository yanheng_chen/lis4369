#!/usr/bin/env python3
import functions as f

# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

def main():
    x = "y" 
    while x == "y":
        f.get_requirements()
        f.estimate_painting_cost()
        x = input("\nDo you want to do another estimation(y/n): ")
    print("\nThank you for using this paint estimator")
    print("\nPlease vist my website: wwww.mywebsite.com")

if __name__ == "__main__":
    main()