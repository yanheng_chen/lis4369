#!/usr/bin/env python3

# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
    print("Paint Estimator")
    print("\nProgram requirements:\n"
        +"1. Calculate home interior patin cost.\n"
        +"2. Must use float data types\n"
        +"3. Must use SQFT_PER_GALLON constant (350).\n"
        +"4. Must use iteration structure (aka \"loop\") \n"
        +"5. Format, right-align numbers, and round to two decimal places\n"
        +"6. Create at least five functions that are called by the program:\n"
        +"\ta. main(): calls at least two other functions: get_requirements() and estimate_painting_cost(). \n"
        +"\tb. get_requirements(): displays the program requirements. \n"
        +"\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions \n"
        +"\td. print_painting_estimate(): displays painting costs.\n"
        +"\te. print_painting_percentage(): displays painting costs percentages.")



def estimate_painting_cost():
    SQFT_PER_GALLON = 350.00
#input
    interior = float(input("Enter total interior sq ft: "))
    ppg = float(input("Enter price per gallon paint: "))
    hpr = float(input("Enter hourly painting rate per sq ft: "))
    
#process
    num_of_gallons = round(interior / SQFT_PER_GALLON, 2)
    paint_cost = round(num_of_gallons * ppg, 3)
    labor_cost = round(hpr * interior, 3)
    total = round(paint_cost + labor_cost, 3)
    pc_percentage = round(paint_cost / total * 100, 2)
    labor_percent = round(labor_cost / total * 100, 2)

    print("Output:")
    print_painting_estimate(interior, SQFT_PER_GALLON, num_of_gallons, ppg, hpr)
    print("\n")
    print_painting_percentage(paint_cost, labor_cost, total, pc_percentage, labor_percent)
#output

def print_painting_estimate(interior, SQFT_PER_GALLON, num_gal, ppg, hpr):
   print("Item", "{0:>20}".format("Amount"))
   print("Total Sq Ft:", "{0:>13}".format("{0:,.2f}".format(interior)))
   print("Sq Ft per Gallon:", "{0:>8}".format("{0:,.2f}".format(SQFT_PER_GALLON)))
   print("Number of Gallons:", "{0:>6}".format("{0:,.2f}".format(num_gal)))
   print("Paint per Gallon", "{0:>2}".format("$"),"{0:>3}".format("{0:,.2f}".format(ppg)))
   print("Labor per Sq Ft", "{0:>3}".format("$"),"{0:>4}".format("{0:,.2f}".format(hpr)))


def print_painting_percentage(paint_cost, labor_cost, total, pc_percentage, labor_percentage):
    print("Cost", "{0:>15}".format("Amount"), "{0:>20}".format("Percentage"))
    print("Paint:","{0:>5}".format("$"), "{0:>5}".format("{0:,.2f}".format(paint_cost)), "{0:>18}%".format(pc_percentage))
    print("Labor:","{0:>5}".format("$"), "{0:>5}".format("{0:,.2f}".format(labor_cost)), "{0:>17}%".format(labor_percentage))
    print("Total:","{0:>5}".format("$"), "{0:>5}".format("{0:,.2f}".format(total)), "{0:>18}".format("100.00%"))