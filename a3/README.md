> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Assignment 3 Requirements:

*Six Parts:*

1. Calculate home interior patin cost.
2. Must use float data types
3. Must use SQFT_PER_GALLON constant (350)
4. Must use iteration structure (aka "loop")
5. Format, right-align numbers, and round to two decimal places
6. Create at least five functions that are called by the program:

    a)main(): calls at least two other functions: get_requirements() and estimate_painting_cost().

    b) get_requirements(): displays the program requirements.

    c) estimate_painting_cost(): calculates interior home painting, and calls print functions

    d) print_painting_estimate(): displays painting costs.

    e) print_painting_percentage(): displays painting costs percentages.

#### Jupyter Notebook files:

* link to A3 ipynb file: [a3_paint_estimator.ipynb](paint_estimator/a3_paint_estimator.ipynb "My A3 Paint Estimator ipynb file")


> ##### Skill sets 4, 5, and 6:

|      Skill set 4 | Skill set 5  |  Skill set 6     |
| ----------- | ----------- |---------------------- |
|    ![Skill_set_4 Calories Calculator](img/ss4.jpg) |   ![Skill_set_5 Python Selction Structures](img/ss5.jpg)     |![Skill_set_6 Python loops](img/ss6.jpg)





#### Assignment Screenshots:

*Screenshot of a3_paint_estimator:


Screenshot of paint_estimator running   part 
![a3_paint_estimator](img/a3_p1.jpg "screenshot of paint estimator")
![a3_paint_estimator](img/a3_p2.jpg "screenshot of paint estimator")


 Screenshot of paint_estimator in Jupyter Notebook 
  ![a3_paint_estimator](img/Jupyter_Notebook/a3_p1.jpg "screenshot of paint estimator in Jupyter Notebook") 
  ![a3_paint_estimator](img/Jupyter_Notebook/a3_p2.jpg "screenshot of paint estimator in Jupyter Notebook") 
  ![a3_paint_estimator](img/Jupyter_Notebook/a3_p3.jpg "screenshot of paint estimator in Jupyter Notebook") 
  ![a3_paint_estimator](img/Jupyter_Notebook/a3_p4.jpg "screenshot of paint estimator in Jupyter Notebook") 