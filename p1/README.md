> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Project 1 Requirements:

*Five Parts:*

1. Run demo.py.
2. If errors, more than likely missing installations
3. Test Python Package Installer: pip freeze
4. Research how to do the following installations:
    a. pandas (only if missing)
    b. pandas-datareader (only if missing)
    c. matplotlib (only if missing)
5. Create three functions that are called by the program:

    a)main(): calls at least two other functions: get_requirements() and data_analysis().

    b) get_requirements(): displays the program requirements.

    c) data_analysis(): displays the following data.

#### Jupyter Notebook files:

* link to p1 ipynb file: [p1_data_analysis_1.ipynb](data_analysis_1/data_analysis.ipynb "My P1 Data Analysis 1 ipynb file")


> ##### Skill sets 7, 8, and 9:

|      Skill set 7 | Skill set 8  |  Skill set 9     |
| ----------- | ----------- |---------------------- |
|    ![Skill_set_7 Using lists](img/ss7_p1.png) |   ![Skill_set_8 Using Tuples](img/ss8.png)     |![Skill_set_8 Using Sets](img/ss9.png)
|    ![Skill_set_7 Using lists](img/ss7_p2.png) |        |




#### Assignment Screenshots:

*Screenshot of p1_data_analysis:


Screenshot of Data Analysis 1   
![p1_data_analysis_1](img/p1_1.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/p1_2.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/p1_picture.png "screenshot of data analysis 1")


 Screenshot of Data Analysis 1 in Jupyter Notebook 
  ![p1_data_analysis_1](img/p1_ipynb1.png"screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/p1_ipynb2.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/p1_ipynb3.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/p1_ipynb4.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/p1_ipynb5.png "screenshot of data analysis 1 in Jupyter Notebook") 