import datetime
import pandas_datareader as pdr 
import matplotlib.pyplot as plt 
from matplotlib import style


def get_requirements():
    print("Data Analysis 1")
    print("\nProgram requirements:\n"
        +"1. Run demo.py.\n"
        +"2. If errors, more than likely missing installations.\n"
        +"3. Test Python Package Installer: pip freeze\n"
        +"4. Research how to do the following installations \n"
        +"\tab a. pandas (only if missing)"
        +"\tab b. pandas-datareader (only if missing)"
        +"\tab c. matplotlib (only if missing)"
        +"5. Create at the three functions that are called by the program\n"
        +"\ta. main(): calls at least two other functions: get_requirements() and data_analysis(). \n"
        +"\tb. get_requirements(): displays the program requirements. \n"
        +"\tc. data_analysis(): displays the following data. \n")




def data_analysis():
    start = datetime.datetime(2010,1,1)
    end = datetime.datetime(2021,10,10)

    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")

    print(len(df.index))

    print(df.columns)

    print()

    print("Print data frame: ")
    print(df)

    print("\nPrint first five lines: ")
    print(df.head(5))

    print("\nPrint last five lines: ")
    print(df.tail(5))

    print("\nPrint first two lines: ")
    print(df.head(2))

    print("\nPrint last two lines: ")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()  