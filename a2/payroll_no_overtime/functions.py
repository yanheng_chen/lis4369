#!/usr/bin/env python3

# Developer: Yanheng Chen
# Course: LIS4369
# Semester: Fall 2021

# Program requirements:
def get_requirements():
    print("Payroll Calculator")
    print("\nProgram requirements:\n"
        +"1. Must use float data type for user input.\n"
        +"2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
        +"3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
        +"4. Must format currency with dollar sign, and round to two decimal places. \n"
        +"5. Create at least three functions that are called by the program:\n"
        +"\ta. main(): calls at least two other functions. \n"
        +"\tb. get_requirements(): displays the program requirements. \n"
        +"\tc. calculate_payroll(): calculates an individual one-week paycheck. \n")

def print_pay(base_pay, overtime_pay, holiday_pay, gross_pay):
    print("\nOutput: ")
    print("{0:<10}${1:,.2f}".format('Base:', base_pay))
    print("{0:<10}${1:,.2f}".format('Overtime:', overtime_pay))
    print("{0:<10}${1:,.2f}".format('Holiday:', holiday_pay))
    print("{0:<10}${1:,.2f}".format('Gross:', gross_pay))

def calculate_payroll():
#Constants to represent base hours, overtime, and holiday rate
   BASE_HOURS = 40 
   OT_RATE = 1.5
   HOLIDAY_RATE = 2.0
    #input
   print("Input:")
#get user hours worked and hourly pay rate
   hours = float(input("Enter hours worked: "))
   holiday_hours = float(input("Enter holiday hours: "))
   pay_rate = float(input("Enter hourly pay rate: "))

#process
#Calculate and display gross pay
   base_pay = BASE_HOURS * pay_rate
   overtime_hours = hours - BASE_HOURS
# calculate and display gross pay
   if hours > BASE_HOURS:
    #calculate gross pay with overtime
    #calculate overtime pay
       overtime_pay = overtime_hours * pay_rate * OT_RATE
    #calculate holiday pay
       holiday_pay = holiday_hours * pay_rate * HOLIDAY_RATE
    #calculate gross pay
       gross_pay = BASE_HOURS * pay_rate + overtime_pay + holiday_pay
       print_pay(base_pay, overtime_pay, holiday_pay, gross_pay)
   else:
        #calculate gross pay without overtime but include holiday pay
       overtime_pay = 0
       holiday_pay = holiday_hours * pay_rate * HOLIDAY_RATE
       gross_pay = hours * pay_rate + holiday_pay
    
    #display pay
       print_pay(base_pay, overtime_pay, holiday_pay, gross_pay)
