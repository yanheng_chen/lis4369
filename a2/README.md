> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Assignment 2 Requirements:

*Five Parts:*

1. Must use float data type for user input
2. Overtime rate: 1.5 times hourly rate (hours over 40)
3. Holiday rate: 2.0 times hourly rate (all holiday hours)
4. Must format currency with dollar sign, and round to two decimal places
5. Create at least three functions that are called by the program:

    a) main(): calls at least two other functions

    b) get_requirements(): displays the program requirements

    c) calculate_payroll(): calculates an individual one-week paycheck

#### Jupyter Notebook files:

* link to A2 ipynb file: [payroll_calculator.ipynb](payroll_no_overtime/payroll_calculator.ipynb "My A2 Payroll Calculator ipynb file")


> ##### Skill sets 1, 2, and 3:
  * link to skillset 1 square_feet_to_acres ipynb file: [square_feet_to_acres.ipynb](../skill_sets/ss1_square_feet_to_acres/square_feet_to_acres.ipynb "My skillset 1 square_feet_to_acres ipynb file") 
  * link to skillset 2 miles_per_gallon ipynb file: [miles_per_gallon.ipynb](../skill_sets/ss2_mile_per_gallon/ss2_mile_per_gallon.ipynb "My skillset 2 square_feet_to_acres ipynb file") 
  * link to skillset 3 IT_ICT_student_percentage ipynb file: [IT_ICT_student_percentage.ipynb](../skill_sets/ss3_IT_ICT_student_percentage/ss3_IT_ICT_student_percentage.ipynb "My skillset 2 square_feet_to_acres ipynb file")

#### Assignment Screenshots:

*Screenshot of a2_payroll_calculator:

![a2_payroll_calculator](img/payroll_calculator.jpg "screenshot of payroll calculator")

*Screenshot of a2_payroll_calculator in Jupyter Notebook*:

![a2_payroll_calculator in Jupyter Notebook](img/jupyter_nb/payroll_1.jpg)

![a2_payroll_calculator in Jupyter Notebook](img/jupyter_nb/payroll_2.jpg)

![a2_payroll_calculator in Jupyter Notebook](img/jupyter_nb/payroll_3.jpg)

*Screenshot of skillset 1 square_feet_to_acres:

![ss1 square_feet_to_acres](img/square_feet_to_acres.jpg)

*Screenshot of skillset 1 square_feet_to_acres in Jupyter Notebook*:

![ss1 square_feet_to_acres](img/jupyter_nb/ss1.jpg)

![ss1 square_feet_to_acres](img/jupyter_nb/ss1_p2.jpg)

*Screenshot of skillset 2 miles_per_gallon calculator:

![ss2 miles_per_gallon](img/mpg_calculator.jpg)

*Screenshot of skillset 2 miles_per_gallon calculator in Jupyter Notebook *:

![ss2 miles_per_gallon](img/jupyter_nb/ss2.jpg)

![ss2 miles_per_gallon](img/jupyter_nb/ss2_p2.jpg)

*Screenshot of skillset 3 IT_ICT_student_percentage:

![ss3 IT_ICT_student_percentage](img/ICT_IT_student.jpg)

*Screenshot of skillset 3 IT_ICT_student_percentage in Jupyter Notebook *:

![ss3 IT_ICT_student_percentage](img/jupyter_nb/ss3_p1.jpg)

![ss3 IT_ICT_student_percentage](img/jupyter_nb/ss3_p2.jpg)

![ss3 IT_ICT_student_percentage](img/jupyter_nb/ss3_p3.jpg)