> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Project 2 Requirements:

*Two Parts:*

1. Use builtin dataset mtcars from RStudio to complete requirements
2. Backward engineer the required screenshots



* link to P2 file: [lis4369_p2.R](lis4369_p2.R "My P2 file")

#### Assignment Screenshots:

*Screenshot of plot 1 in Project 2:

![Plot_from_plot()](plot.png "Plot from plot() function")

*Screenshot of a1_tip_calculator in Visual Studio Code*:

![Plot_from_qplot()](qplot.png "Plot from qplot() function")

*SScreenshot of a1_tip_calculator in Juoyter Notebook*:

![a1_tip_calculator in Jupyter Notebook](p2.png)



