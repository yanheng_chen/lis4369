> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Assignment 4 Requirements:

*Six Parts:*

1. Run demo.py.
2. If errors, more than likely missing installations
3. Test Python Package Installer: pip freeze
4. Research how to do the following installations:
    a. pandas (only if missing)
    b. pandas-datareader (only if missing)
    c. matplotlib (only if missing)
5. Create three functions that are called by the program:

    a)main(): calls at least two other functions: get_requirements() and data_analysis().

    b) get_requirements(): displays the program requirements.

    c) data_analysis(): displays the following data.

6. Display graph as per instructions w/in demo.py.

#### Jupyter Notebook files:

* link to a4 ipynb file: [a4_data_analysis_2.ipynb](data_analysis_2/data_analysis_2.ipynb "My A2 Data Analysis 2 ipynb file")


> ##### Skill sets 10, 11, and 12:

Skill set 10:
![Skill_set_10 Using dictionaries](img/ss10_1.png)
![Skill_set_10 Using dictionaries](img/ss10_2.png)

Skill set 11:
![Skill_set_11 Randon number generator](img/ss11.png)

SKill set 12:
![Skill_set_12 Temperature conversion program](img/ss12.png)

#### Assignment Screenshots:

*Screenshot of a4_data_analysis_2: 
![p1_data_analysis_1](img/a4_1.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_2.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_3.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_4.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_5.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_6.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_7.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_8.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_9.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_10.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_11.png "screenshot of data analysis 1")
![p1_data_analysis_1](img/a4_12.png "screenshot of data analysis 1")

 Screenshot of Data Analysis 1 in Jupyter Notebook 
  ![p1_data_analysis_1](img/a4_jb1.png"screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb2.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb3.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb4.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb5.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb6.png"screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb7.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb8.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb9.png "screenshot of data analysis 1 in Jupyter Notebook") 
  ![p1_data_analysis_1](img/a4_jb10.png "screenshot of data analysis 1 in Jupyter Notebook") 