> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**


# Course LIS4369 Extensible Enterprise Solutions (Python/R Data Analytics/Visualization)

## Yanheng Chen


### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Create *a1_tip_calculator* application
    - Create *a1 tip calculator* Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create *a2_payroll_calculator* application
    - Create *a2_payroll_calculator* Jupyter Notebook
    - Create *skill_set_1_square_feet_to_acres* application
    - Create *skill_set_1_square_feet_to_acres* Jupyter Notebook
    - Create *skill_set_2_miles_per_gallon* application
    - Create *skill_set_2_miles_per_gallon* Jupyter Notebook
    - Create *skill_set_3_IT_ICT_student_percentage* application
    - Create *skill_set_3_IT_ICT_student_percentage* Jupyter Notebook

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Calculate interior paint cost
    - Create five functions for paint estimator program
    - Finish skillsets 4, 5, and 6

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Finish skillsets 10, 11, and 12
    - Analyze and visualize titanic victims dataset

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Analyze and visualize titanic victims dataset using R
    - Went through R tutorial using RStudio

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Install python packages: pandas-datareader and matplotlib
    - Use the packages to analyze data
    - Finish skillsets 7, 8, and 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Backward engineered the required results
    - Used R to analyze mtcars dataset from RStudios