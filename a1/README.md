> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:

    a) this assignment 

    b) the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of a1_tip calculator application running
* link to A1 ipynb file: [tip_calculator.ipynb](a1_tip_calculator/tip_calculator.ipynb "My A1 tip calculaor ipynb file")
* git commands with short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - initialize the repo locally
2. git status - describes the state of the current working directory and the staging area
3. git add - tells git to include update in the next commit
4. git commit - create a new commit with the current contents and giving a message describing the changes
5. git push - upload files from local to remote repo
6. git pull - download files from remote to local repo
7. git clone - target a repo and make a copy of that repo

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator in IDLE*:

![a1_tip_calculator in IDLE](img/a1_tip_calculator_idle.png "screenshot of tip calculator in idle")

*Screenshot of a1_tip_calculator in Visual Studio Code*:

![a1_tip_calculator in Visual Studio Code](img/a1_tip_calculator_vsc.png)

*SScreenshot of a1_tip_calculator in Juoyter Notebook*:

![a1_tip_calculator in Jupyter Notebook](img/a1_tip_calculator_jupyter_notebook.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/yanheng_chen/bitbucketstationlocations/ "Bitbucket Station Locations")

